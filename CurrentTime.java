import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class CurrentTime extends Watch {
  private static final DateTimeFormatter FMTR = DateTimeFormatter.ofPattern("HH:mm:ss");
  private              boolean           running   = true;


  @Override
  public String getValue() {
    return FMTR.format(LocalDateTime.now());
  }

  @Override
  public String getDetail() {
    return "Current time";
  }

  @Override
  public boolean isRunning() {
    return running;
  }

  @Override
  synchronized void start() {
    running = true;
    notifyAll();
  }

  @Override
  void stop() {
    running = false;
  }

  @Override
  void reset() {
    // no-op
  }
}

