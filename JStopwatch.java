import java.awt.FlowLayout;
import java.lang.reflect.InvocationTargetException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class JStopwatch extends Thread {
  private static final JFrame     FRAME   = new JFrame("JStopwatch");
  private static final FlowLayout LAYOUT  = new FlowLayout();
  private static final JLabel     VALUE   = new JLabel();
  private static final JLabel     DETAIL  = new JLabel();

  private static final JButton    RESET   = new JButton("RESET");
  private static final JButton    MODE    = new JButton("MODE");
  private static final JButton    TOGGLE  = new JButton();
  private static       Thread     thread;

  private static final Watch[]    WATCHES = new Watch[1];
  private static       Watch      current;
  private static       int        curMode = -1;


  /**
   * Update the value every second.
   */
  @Override
  public void run() {
    while ( true ) {
      VALUE.setText(current.getValue());
      FRAME.pack();

      try {
        Thread.sleep(1000);
        // guarded block to minimize resource consumption
        while ( ! current.isRunning() ) {
          synchronized(thread) {
            thread.wait();
          }
        }
      } catch ( InterruptedException e ) { }
    }
  }

  /**
   * Initialize watches.
   */
  private static void setupWatches() {
    WATCHES[0] = new CurrentTime();
    cycleMode();
  }

  /**
   * Cycle between modes on the stopwatch.
   */
  private static void cycleMode() {
    if ( curMode >= WATCHES.length - 1 ) {
      current = WATCHES[0];
      curMode = 0;
    } else {
      current = WATCHES[++curMode];
      updateToggleText();
    }

    DETAIL.setText(current.getDetail());
    FRAME.pack();
  }

  /**
   * Update the text on the toggle button.
   */
  private static void updateToggleText() {
    if ( current.isRunning() ) {
      TOGGLE.setText("STOP");
    } else {
      TOGGLE.setText("START");
    }
    FRAME.pack();
  }

  public static void main(String[] args) {
    try {
      javax.swing.SwingUtilities.invokeAndWait(new FrameInit());
    } catch ( InterruptedException e ) {
      System.exit(0);
    } catch ( InvocationTargetException e ) {
      e.printStackTrace();
      System.exit(1);
    }

    setupWatches();
    updateToggleText();

    thread = new Thread(new JStopwatch());
    thread.start();
  }

  public static class ResetAction implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      current.reset();
    }
  }

  public static class ModeAction implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      cycleMode();
    }
  }

  public static class ToggleAction implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
      if ( current.isRunning() ) {
        current.stop();
      } else {
        current.start();
        synchronized(thread) {
          thread.notifyAll();
        }
      }
      updateToggleText();
    }
  }

  public static class FrameInit implements Runnable {
    @Override
    public void run() {
      // ask for window decorations provided by the look and feel.
      JFrame.setDefaultLookAndFeelDecorated(true);

      FRAME.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      FRAME.setLayout(LAYOUT);

      // start in the center
      FRAME.setLocationRelativeTo(null);

      FRAME.add(VALUE);
      FRAME.add(DETAIL);
      FRAME.add(RESET);
      FRAME.add(MODE);
      FRAME.add(TOGGLE);
      FRAME.setVisible(true);

      RESET.addActionListener(new ResetAction());
      MODE.addActionListener(new ModeAction());
      TOGGLE.addActionListener(new ToggleAction());
    }
  }
}


