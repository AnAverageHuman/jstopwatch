public abstract class Watch {
  private boolean running = false;

  // values to be displayed
  public abstract String getValue();

  public abstract String getDetail();

  public boolean isRunning() {
    return running;
  }

  abstract void start();

  abstract void stop();

  abstract void reset();
}

